import numpy as np



def eucl_std_score(acc,unacc,g0_weight_sorted,g1_weight_sorted, k1, dist0, dist1):
    data_distribution_0 = [acc[i0[0]] for i0 in g0_weight_sorted[:k1]]  # data distribution of the class 0 more near to test object
    data_distribution_1 = [unacc[i0[0]] for i0 in g1_weight_sorted[:k1]]
    #print(data_distribution_0)
    #print(data_distribution_1)
    std_group_0 = np.std(data_distribution_0)
    std_group_1 = np.std(data_distribution_1)
    #print(np.std(data_distribution_0), np.std(data_distribution_1))
    #print(dist0, dist1)
    score_0=dist0 * (std_group_0 + 1.)
    score_1=dist1 * (std_group_1 + 1.)
    if score_0 < score_1:
        return 0
    else:
        return 1