import itertools
import math
# from mayavi import mlab
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import mark_inset, zoomed_inset_axes
from mpl_toolkits.mplot3d import axes3d
# from matplotlib import cm
from metric import eucl_std_score


def create_small_mst(g0_weight_sorted, g1_weight_sorted, gamma_index, acc, unacc):
    small_g0 = nx.Graph()
    small_g0.add_nodes_from([i[0] for i in g0_weight_sorted[:gamma_index]])
    edges_couples = itertools.product(nx.nodes(small_g0), nx.nodes(small_g0))
    for u, v in (edges_couples):
        small_g0.add_edge(u, v, weight=distance.euclidean(acc[u], acc[v]))

    small_mst0 = nx.minimum_spanning_tree(small_g0)

    small_g1 = nx.Graph()
    small_g1.add_nodes_from([i[0] for i in g1_weight_sorted[:gamma_index]])
    edges_couples = itertools.product(nx.nodes(small_g1), nx.nodes(small_g1))
    for u, v in (edges_couples):
        small_g1.add_edge(u, v, weight=distance.euclidean(unacc[u], unacc[v]))

    small_mst1 = nx.minimum_spanning_tree(small_g1)
    return small_mst0, small_mst1


# def mayavi3d_mst(test,small_mst0, small_mst1, theta0, theta1, path, acc, unacc):
from scipy.spatial import distance


# def mayavi3d_mst():
#     gamma_index = 15
#     all_distance_positive_train, all_distance_negative_train = [], []
#     g0 = nx.Graph()
#     g1 = nx.Graph()
#     acc = np.random.random_sample((150, 3))
#     unacc = np.random.random_sample((150, 3))
#     threshold_list = [0.4, 0.6, 0.9]
#     test_list = np.random.random_sample((1, 3))
#     #test_list= [np.asarray(acc[0]+0.01)]
#
#     for i in range(0, len(acc)):
#         g0.add_node(i)
#
#     for i in range(0, len(unacc)):
#         g1.add_node(i)
#
#     # try:
#     #     train_data = np.load(path + 'features.npy')
#     #     train_labels = np.load(path + 'labels.npy')
#     #     test_data = np.load(path + 'all_test_features.npy')
#     #     test_labels = np.load(path + 'all_test_labels.npy')
#     #
#     # except Exception as e:
#     #     print(e)
#     #     print("Loading datasetes failed")
#     #     exit()
#
#     x0, y0, z0 = zip(*(acc))
#     x1, y1, z1 = zip(*(unacc))
#     #y_test0, y_test1, y_test2 = zip(*(test))
#
#     mlab.figure(1, size=(512, 512), bgcolor=(1, 1, 1))
#     mlab.clf()
#     for label_idx in range(0, 2):
#         if label_idx == 0:
#             color = (0., 0., 1.)
#             pts0 = mlab.points3d(x0, y0, z0, scale_factor=0.01, scale_mode='none',
#                                  color=color, name='Minimum Spanning Tree', resolution=50)
#
#         else:
#             pass
#             #color = (1., 0., 0.)
#             # pts1 = mlab.points3d(x1, y1, z1, scale_factor=0.03, scale_mode='none',color=color, name='Minimum Spanning Tree',resolution=50)
#
#     # Add the connections and display them with tubes.
#     mlab.outline(color=(0, 0, 0))
#     for test in test_list:
#         y_test0, y_test1, y_test2 = test[0],test[1],test[2]
#         for node0 in nx.nodes(g0):
#             all_distance_positive_train.append([node0, distance.euclidean(test, acc[node0])])
#
#         for node1 in nx.nodes(g1):
#             all_distance_negative_train.append([node1, distance.euclidean(test, unacc[node1])])
#
#         g0_weight_sorted = sorted(all_distance_positive_train, key=lambda x: x[1])
#
#         g1_weight_sorted = sorted(all_distance_negative_train, key=lambda x: x[1])
#
#         small_mst0, small_mst1 = create_small_mst(g0_weight_sorted, g1_weight_sorted, gamma_index, acc, unacc)
#
#         dat = sorted(small_mst0.edges(data=True), key=lambda x: x[2]['weight'])
#         threshold_0 = int(math.floor((small_mst0.size()) * threshold_list[0]))
#         threshold_1 = int(math.floor((small_mst0.size()) * threshold_list[1]))
#         threshold_2 = int(math.floor((small_mst0.size()) * threshold_list[2]))
#
#         theta0 = dat[threshold_0][2]['weight']
#         theta1 = dat[threshold_1][2]['weight']
#         theta2 = dat[threshold_2][2]['weight']
#
#         ##################
#         fig = plt.figure()
#         ax = fig.gca(projection='3d')
#
#
#         ax.scatter(acc[:, 0], acc[:, 1],acc[:, 2], 'blue',s=50)
#
#         ax.scatter(test_list[:, 0], test_list[:, 1],test_list[:, 2],'red',s=50)
#
#
#         for edge in small_mst0.edges:
#             i, j = edge
#             ax.plot3D([x0[i],x0[j]],[y0[i],y0[j]],[z0[i],z0[j]], 'black', lw=5,solid_capstyle='round', alpha=0.9)
#             ax.plot3D([x0[i],x0[j]],[y0[i],y0[j]],[z0[i],z0[j]], 'blue', lw=10,solid_capstyle='round', alpha=0.6)
#             ax.plot3D([x0[i], x0[j]], [y0[i], y0[j]], [z0[i], z0[j]], 'blue', lw=20, solid_capstyle='round', alpha=0.2)
#
#         #plt.savefig("graph.svg")
#         plt.show()
#
#
#         exit()
#
#         dist0, dist0_euclidean = [], []
#         epsilon = 0.00000001
#         for c0 in small_mst0.edges(data=True):
#             x_j = acc[c0[1]]
#             x_i = acc[c0[0]]
#             x = test
#             # if c0[2]['weight'] <= theta:
#             denominator = distance.sqeuclidean(x_j, x_i) + epsilon
#             degree = (np.dot(np.transpose(np.subtract(x_j, x_i)), np.subtract(x, x_i))) / denominator
#             degree = (np.abs(degree))
#             if 0. <= degree <= 1.:
#                 projection_point = np.add(x_i, (
#                     np.dot(((np.dot(np.transpose(np.subtract(x_j, x_i)), np.subtract(x, x_i))) / denominator),
#                            (np.subtract(x_j, x_i)))))
#                 dist0.append(distance.euclidean(x, projection_point))
#             else:
#                 dist0_euclidean.append(min(distance.euclidean(x_j, x), distance.euclidean(x_i, x)))
#
#         if dist0:
#             min_dist0 = min(dist0)
#             dist0_sorted = sorted(dist0)
#
#             if min_dist0 <= theta0:
#                 pt_test = mlab.points3d(y_test0, y_test1, y_test2, scale_factor=0.01, scale_mode='none',
#                                         color=(0., 1., 0.), resolution=50)
#
#             if min_dist0 > theta0 and min_dist0 < theta2:
#                 pt_test = mlab.points3d(y_test0, y_test1, y_test2, scale_factor=0.01, scale_mode='none',
#                                         color=(1., 1., 0.), resolution=50)
#
#
#             elif min_dist0 >= theta2:
#                 pt_test = mlab.points3d(y_test0, y_test1, y_test2, scale_factor=0.01, scale_mode='none',
#                                         color=(1., 0., 0.), resolution=50)
#
#
#
#         elif not dist0:
#             min_dist0_euclidean = min(dist0_euclidean)
#             if min_dist0_euclidean <= theta0:
#                 pt_test = mlab.points3d(y_test0, y_test1, y_test2, scale_factor=0.01, scale_mode='none',
#                                         color=(0., 1., 0.), resolution=50)
#
#             if min_dist0_euclidean > theta0 and min_dist0_euclidean < theta2:
#                 pt_test = mlab.points3d(y_test0, y_test1, y_test2, scale_factor=0.01, scale_mode='none',
#                                         color=(1., 1., 0.), resolution=50)
#
#
#             elif min_dist0_euclidean >= theta2:
#                 pt_test = mlab.points3d(y_test0, y_test1, y_test2, scale_factor=0.01, scale_mode='none',
#                                         color=(1., 0., 0.), resolution=50)
#
#
#         pts0.mlab_source.dataset.lines = np.array([i for i in small_mst0.edges])
#
#         #pts1.mlab_source.dataset.lines = np.array([i for i in small_mst1.edges])
#
#
#
#         mlab.pipeline.surface(mlab.pipeline.tube(pts0, tube_radius=theta0/5, tube_sides=20), color=(0., 0., 1.), opacity=0.9)
#         mlab.pipeline.surface(mlab.pipeline.tube(pts0, tube_radius=theta1/5 , tube_sides=20), color=(1., .5, 0.),opacity=0.4)
#         mlab.pipeline.surface(mlab.pipeline.tube(pts0, tube_radius=theta2/5 , tube_sides=20), color=(1., 0., 0.),opacity=0.2)
#
#
#         # mlab.pipeline.surface(mlab.pipeline.tube(pts1, tube_radius=0.004), color=(0., 0., 1.), opacity=1)
#         # mlab.pipeline.surface(mlab.pipeline.tube(pts1, tube_radius=0.001), color=(0., 0., 1.), opacity=0.5)
#         # mlab.pipeline.surface(mlab.pipeline.tube(pts1, tube_radius=0.02), color=(0., 0., 1.), opacity=0.2)
#
#
#
#         mlab.outline(color=(1, 0, 0))
#         mlab.view(180, -30, 0.7)
#         mlab.show()


def mat3dplot():
        gamma_index = 8
        all_distance_positive_train, all_distance_negative_train = [], []
        g0 = nx.Graph()
        g1 = nx.Graph()
        acc = np.random.rand(30, 3)
        unacc = np.random.rand(15, 3)
        threshold_list = [0.1, 0.4, 0.8]
        test_list = np.random.rand(20, 3)
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        for i in range(0, len(acc)):
            g0.add_node(i)

        for i in range(0, len(unacc)):
            g1.add_node(i)

        x0, y0, z0 = zip(*(acc))
        x1, y1, z1 = zip(*(unacc))
        # y_test0, y_test1, y_test2 = zip(*(test))


        for label_idx in range(0, 2):
            if label_idx == 0:
                ax.scatter(acc[:, 0], acc[:, 1], acc[:, 2], 'blue', s=30)

            else:
                pass
                # color = (1., 0., 0.)
                # pts1 = mlab.points3d(x1, y1, z1, scale_factor=0.03, scale_mode='none',color=color, name='Minimum Spanning Tree',resolution=50)

        # Add the connections and display them with tubes.

        for test in test_list:
            y_test0, y_test1, y_test2 = test[0], test[1], test[2]
            for node0 in nx.nodes(g0):
                all_distance_positive_train.append([node0, distance.euclidean(test, acc[node0])])

            for node1 in nx.nodes(g1):
                all_distance_negative_train.append([node1, distance.euclidean(test, unacc[node1])])

            g0_weight_sorted = sorted(all_distance_positive_train, key=lambda x: x[1])

            g1_weight_sorted = sorted(all_distance_negative_train, key=lambda x: x[1])

            small_mst0, small_mst1 = create_small_mst(g0_weight_sorted, g1_weight_sorted, gamma_index, acc, unacc)

            dat = sorted(small_mst0.edges(data=True), key=lambda x: x[2]['weight'])
            threshold_0 = int(math.floor((small_mst0.size()) * threshold_list[0]))
            threshold_1 = int(math.floor((small_mst0.size()) * threshold_list[1]))
            threshold_2 = int(math.floor((small_mst0.size()) * threshold_list[2]))

            theta0 = dat[threshold_0][2]['weight']
            theta1 = dat[threshold_1][2]['weight']
            theta2 = dat[threshold_2][2]['weight']

            dat1 = sorted(small_mst1.edges(data=True), key=lambda x: x[2]['weight'])
            threshold_10 = int(math.floor((small_mst1.size()) * threshold_list[0]))
            threshold_11 = int(math.floor((small_mst1.size()) * threshold_list[1]))
            threshold_12 = int(math.floor((small_mst1.size()) * threshold_list[2]))

            theta10 = dat1[threshold_10][2]['weight']
            theta11 = dat1[threshold_11][2]['weight']
            theta12 = dat1[threshold_12][2]['weight']

            ##################





            dist0,dist1,dist1_euclidean, dist0_euclidean = [], [], [], []
            epsilon = 0.00000001

            for c0 in small_mst0.edges(data=True):
                x_j = acc[c0[1]]
                x_i = acc[c0[0]]
                x = test
                # if c0[2]['weight'] <= theta:
                denominator = distance.sqeuclidean(x_j, x_i) + epsilon
                degree = (np.dot(np.transpose(np.subtract(x_j, x_i)), np.subtract(x, x_i))) / denominator
                degree = (np.abs(degree))
                if 0. <= degree <= 1.:
                    projection_point = np.add(x_i, (
                        np.dot(((np.dot(np.transpose(np.subtract(x_j, x_i)), np.subtract(x, x_i))) / denominator),
                               (np.subtract(x_j, x_i)))))
                    dist0.append(distance.euclidean(x, projection_point))
                else:
                    dist0_euclidean.append(min(distance.euclidean(x_j, x), distance.euclidean(x_i, x)))

            for c1 in small_mst1.edges(data=True):
                x_j = unacc[c1[1]]
                x_i = unacc[c1[0]]
                x = test
                # if c1[2]['weight'] < theta1:
                denominator = distance.sqeuclidean(x_j, x_i) + epsilon
                degree = (np.dot(np.transpose(np.subtract(x_j, x_i)), np.subtract(x, x_i))) / denominator
                degree = (np.abs(degree))
                if 0. <= degree <= 1.:
                    projection_point = np.add(x_i, (
                        np.dot(((np.dot(np.transpose(np.subtract(x_j, x_i)), np.subtract(x, x_i))) / denominator),
                               (np.subtract(x_j, x_i)))))
                    dist1.append(distance.euclidean(x, projection_point))
                else:
                    dist1_euclidean.append(min(distance.euclidean(x_j, x), distance.euclidean(x_i, x)))

                # else:
                # dist1_euclidean.append(min(distance.euclidean(x_j, x), distance.euclidean(x_i, x)))


            if dist0 and dist1:
                min_dist0 = min(dist0)
                min_dist1 = min(dist1)
                dist0_sorted = sorted(dist0)
                dist1_sorted = sorted(dist1)

                if min_dist0 <= theta0 and min_dist1 <= theta10:
                    won='both'
                    predicted = eucl_std_score(acc, unacc, g0_weight_sorted, g1_weight_sorted, 10, min_dist0, min_dist1)
                    ax.scatter(test[0], test[1], test[2], marker='^', c='blue' if predicted==0 else 'green', s=40)
                    ax.scatter(test[0], test[2], marker='^', c='blue' if predicted==0 else 'green',   zdir='y', zs=1.5)
                    ax.scatter(test[1], test[2], marker='^', c='blue' if predicted==0 else 'green', zdir='x',   zs=-0.5)
                    ax.scatter(test[0], test[1], marker='^', c='blue' if predicted==0 else 'green', zdir='z' , zs=-1.5)


                if min_dist0 <= theta0 and min_dist1 > theta11:
                    won='0'
                    ax.scatter(test[0], test[1], test[2], marker='^', c='blue', s=40)
                    ax.scatter(test[0], test[2], marker='^', c='blue', zdir='y' , zs=1.5)
                    ax.scatter(test[1], test[2], marker='^', c='blue' , zdir='x',  zs=-0.5)
                    ax.scatter(test[0], test[1], marker='^', c='blue', zdir='z', zs=-1.5)


                elif min_dist0 > theta0 and min_dist1 <= theta11:
                    won='1'
                    ax.scatter(test[0], test[1], test[2], marker='^', c='green' , s=40)
                    ax.scatter(test[0], test[2], marker='^', c='green',   zdir='y', zs=1.5)
                    ax.scatter(test[1], test[2], marker='^', c='green',   zdir='x', zs=-0.5)
                    ax.scatter(test[0], test[1], marker='^', c='green', zdir='z', zs=-1.5)

                elif min_dist0 > theta0 and min_dist1 > theta11:
                    won='both'
                    predicted = eucl_std_score(acc, unacc, g0_weight_sorted, g1_weight_sorted, 10, min_dist0, min_dist1)
                    ax.scatter(test[0], test[1], test[2], marker='^', c='blue' if predicted == 0 else 'green', s=40)
                    ax.scatter(test[0], test[2], marker='^', c='blue' if predicted == 0 else 'green', zdir='y', zs=1.5)
                    ax.scatter(test[1], test[2], marker='^', c='blue' if predicted == 0 else 'green', zdir='x', zs=-0.5)
                    ax.scatter(test[0], test[1], marker='^', c='blue' if predicted == 0 else 'green', zdir='z', zs=-1.5)

            # if dist0:
            #     min_dist0 = min(dist0)
            #     dist0_sorted = sorted(dist0)
            #
            #     if min_dist0 <= theta0:
            #         pass
            #         # ax.scatter(test_list[:, 0], test_list[:, 1], test_list[:, 2], c='green', s=30)
            #         # ax.scatter(test_list[:, 0], test_list[:, 2], c='green', zdir='y', zs=1.5)
            #         # ax.scatter(test_list[:, 1], test_list[:, 2], c='green', zdir='x', zs=-0.5)
            #         # ax.scatter(test_list[:, 0], test_list[:, 1], c='green', zdir='z', zs=-1.5)
            #
            #     if min_dist0 > theta0 and min_dist0 < theta2:
            #         pass
            #         # ax.scatter(test_list[:, 0], test_list[:, 1], test_list[:, 2], c='orange', s=30)
            #         # ax.scatter(test_list[:, 0], test_list[:, 2], c='orange', zdir='y', zs=1.5)
            #         # ax.scatter(test_list[:, 1], test_list[:, 2], c='orange', zdir='x', zs=-0.5)
            #         # ax.scatter(test_list[:, 0], test_list[:, 1], c='orange', zdir='z', zs=-1.5)
            #
            #
            #     elif min_dist0 >= theta2:
            #         ax.scatter(test[0], test[1], test[2], c='red', s=30)
            #         ax.scatter(test[0], test[2], c='red', zdir='y', zs=1.5)
            #         ax.scatter(test[1], test[2], c='red', zdir='x', zs=-0.5)
            #         ax.scatter(test[0], test[1], c='red', zdir='z', zs=-1.5)
            #         break
            #
            #
            # elif not dist0:
            #     min_dist0_euclidean = min(dist0_euclidean)
            #     if min_dist0_euclidean <= theta0:
            #         pass
            #         # ax.scatter(test_list[:, 0], test_list[:, 1], test_list[:, 2], c='green', s=30)
            #
            #     if min_dist0_euclidean > theta0 and min_dist0_euclidean < theta2:
            #         pass
            #         # ax.scatter(test_list[:, 0], test_list[:, 1], test_list[:, 2], c='orange', s=30)
            #
            #
            #     elif min_dist0_euclidean >= theta2:
            #         pass
            #         # ax.scatter(test_list[:, 0], test_list[:, 1], test_list[:, 2], c='red', s=30)

            break
            all_distance_positive_train, all_distance_negative_train=[], []

        for edge in small_mst0.edges:
            i, j = edge
            ax.plot3D([x0[i], x0[j]], [y0[i], y0[j]], [z0[i], z0[j]], c='black', lw=2.5, solid_capstyle='round')

            ax.plot3D([x0[i], x0[j]], [y0[i], y0[j]], [z0[i], z0[j]], c='blue', lw=theta0 * 100, solid_capstyle='round',
                      alpha=0.6)
            ax.plot3D([x0[i], x0[j]], [y0[i], y0[j]], [z0[i], z0[j]], c='blue', lw=theta1 * 100, solid_capstyle='round',
                      alpha=0.3)
            ax.plot3D([x0[i], x0[j]], [y0[i], y0[j]], [z0[i], z0[j]], c='blue', lw=theta2 * 100, solid_capstyle='round',
                      alpha=0.1)

            ax.plot([x0[i], x0[j]], [z0[i], z0[j]], c='black', zdir='y', zs=1.5)
            ax.plot([y0[i], y0[j]], [z0[i], z0[j]], c='black', zdir='x', zs=-0.5)
            ax.plot([x0[i], x0[j]], [y0[i], y0[j]], c='black', zdir='z', zs=-1.5)

            ax.scatter(x0[i], z0[i], c='blue', zdir='y', zs=1.5, s=30)
            ax.scatter(y0[i], z0[i], c='blue', zdir='x', zs=-0.5, s=30)
            ax.scatter(x0[i], y0[i], c='blue', zdir='z', zs=-1.5, s=30)

            ax.scatter(x0[j], z0[j], c='blue', zdir='y', zs=1.5, s=30)
            ax.scatter(y0[j], z0[j], c='blue', zdir='x', zs=-0.5, s=30)
            ax.scatter(x0[j], y0[j], c='blue', zdir='z', zs=-1.5, s=30)


        for edge in small_mst1.edges:
            i, j = edge
            ax.plot3D([x1[i], x1[j]], [y1[i], y1[j]], [z1[i], z1[j]], c='black', lw=2.5, solid_capstyle='round')

            ax.plot3D([x1[i], x1[j]], [y1[i], y1[j]], [z1[i], z1[j]], c='green', lw=theta10 * 100, solid_capstyle='round',
                      alpha=0.6)
            ax.plot3D([x1[i], x1[j]], [y1[i], y1[j]], [z1[i], z1[j]], c='green', lw=theta11 * 100, solid_capstyle='round',
                      alpha=0.3)
            ax.plot3D([x1[i], x1[j]], [y1[i], y1[j]], [z1[i], z1[j]], c='green', lw=theta12 * 100, solid_capstyle='round',
                      alpha=0.1)

            ax.plot([x1[i], x1[j]], [z1[i], z1[j]], c='black', zdir='y', zs=1.5)
            ax.plot([y1[i], y1[j]], [z1[i], z1[j]], c='black', zdir='x', zs=-0.5)
            ax.plot([x1[i], x1[j]], [y1[i], y1[j]], c='black', zdir='z', zs=-1.5)

            ax.scatter(x1[i], z1[i], c='green', zdir='y', zs=1.5, s=30)
            ax.scatter(y1[i], z1[i], c='green', zdir='x', zs=-0.5, s=30)
            ax.scatter(x1[i], y1[i], c='green', zdir='z', zs=-1.5, s=30)

            ax.scatter(x1[j], z1[j], c='green', zdir='y', zs=1.5, s=30)
            ax.scatter(y1[j], z1[j], c='green', zdir='x', zs=-0.5, s=30)
            ax.scatter(x1[j], y1[j], c='green', zdir='z', zs=-1.5, s=30)

        ax.set_xlabel('X')
        ax.set_ylabel('Y')
        ax.set_zlabel('Z')
        ax.set_yticklabels([])
        ax.set_xticklabels([])
        ax.set_zticklabels([])
        plt.savefig("graph "+won+str(np.random.rand())+".pdf")
        # ax.set_xlim3d(-.2, 1.)
        # ax.set_ylim3d(-.2, 1.)
        # ax.set_zlim3d(-.2, 1.)
        #plt.show()


mat3dplot()
#mayavi3d_mst()
