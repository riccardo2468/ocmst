# -*- coding: utf-8 -*-
from __future__ import division  # to force division to floating point
import sys
import time
import itertools
import math
import random
from collections import Counter
from scipy.spatial import distance
import networkx as nx
import numpy as np
import torch
#import plot_boundaries
from metric import eucl_std_score
epsilon=sys.float_info.epsilon



def pairwise_distances(x, y):
    mmp1 = torch.stack([x] * y.size(0))
    mmp2 = torch.stack([y] * x.size(0)).transpose(0, 1)
    dist = torch.sqrt(torch.sum((mmp1 - mmp2) ** 2, 2))
    return dist


def pred_assing(pred, TP, TN_1, FP, FP_1, FN, FN_1,TP_1, TN, y_true):
    if pred == 0:
        if y_true == 0:
            TP = TP + 1
            TN_1 = TN_1 + 1
        elif y_true == 1:
            FP = FP + 1
            FN_1 = FN_1 + 1
    else:
        if y_true == 0:
            FP_1 = FP_1 + 1
            FN = FN + 1
        elif y_true == 1:
            TP_1 = TP_1 + 1
            TN = TN + 1
    return TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN



def create_small_mst(g0_weight_sorted, gamma, acc):
    small_g0 = nx.Graph()
    small_g0.add_nodes_from([i[0] for i in g0_weight_sorted[:gamma]])
    edges_couples = itertools.product(nx.nodes(small_g0), nx.nodes(small_g0))
    for u, v in (edges_couples):
        small_g0.add_edge(u, v, weight=distance.euclidean(acc[u], acc[v]))

    small_mst0 = nx.minimum_spanning_tree(small_g0)

    return small_mst0


def main_OcMst(acc,X_test,y_test, path):
    threshold_list = [0.1, 0.5, 0.8]
    gamma=8


    print("#####################")
    print("Gamma= ", gamma, "Thr: ",threshold_list)
    g0 = nx.Graph()

    for i in range(0, len(acc)):
        g0.add_node(i)

    # prediction test
    TN = 0
    FP = 0
    TP = 0
    FN = 0

    TN_1 = 0
    FP_1 = 0
    TP_1 = 0
    FN_1 = 0
    y_pred = []
    start_time=time.time()
    for j in (range(0, len(X_test))):
        new_vectorize = X_test[j]
        all_distance_positive_train = []
        sub_g0=[]

        tr=time.time()
        dist_acc = pairwise_distances(torch.FloatTensor(acc).cuda(), torch.FloatTensor(X_test[j]).view(1, len(X_test[j])).cuda())
        dist_acc = dist_acc.cpu().tolist()

        new_acc = []
        for tens_list in dist_acc:
            for node0, t1 in enumerate(tens_list):
                new_acc.append([node0, t1])

        all_distance_positive_train.append(new_acc)
        g0_weight_sorted = sorted(all_distance_positive_train[0], key=lambda x: x[1])


        small_mst0 = create_small_mst(g0_weight_sorted, gamma, acc)
        # print(small_mst0.edges(data=True))
        # print([acc[nodes0] for nodes0 in nx.nodes(small_mst0)])
        # print([unacc[nodes0] for nodes0 in nx.nodes(small_mst1)])
        # print(new_vectorize)

        #Search all edges incoming/outgoing to node (node is the minimum with our test  object)
        # for edge0 in g0.edges(data=True):
        #     if index_min_positive_train == edge0[0] or index_min_positive_train == edge0[1]:
        #             sub_g0.append([edge0[0], edge0[1], edge0[2]['weight']])
        #
        # for edge1 in g1.edges(data=True):
        #     if index_min_negative_train == edge1[0] or index_min_negative_train == edge1[1]:
        #             sub_g1.append([edge1[0], edge1[1], edge1[2]['weight']])



        #n_ary0, n_ary1 = create_n_ary(sub_g0, sub_g1, gamma_index)

        # Search thresholds into small mst0/1
        dat = sorted(small_mst0.edges(data=True), key=lambda x: x[2]['weight'])
        threshold_0 = int(math.floor((small_mst0.size()) * threshold_list[0]))
        threshold_1 = int(math.floor((small_mst0.size()) * threshold_list[1]))
        threshold_2 = int(math.floor((small_mst0.size()) * threshold_list[2]))

        theta0 = dat[threshold_0][2]['weight']
        theta1 = dat[threshold_1][2]['weight']
        theta2 = dat[threshold_2][2]['weight']

        # search projection
        dist0 = []
        dist0_euclidean = []
        for c0 in small_mst0.edges(data=True):
            x_j = acc[c0[1]]
            x_i = acc[c0[0]]
            x = new_vectorize
            #if c0[2]['weight'] <= theta:
            denominator = distance.sqeuclidean(x_j, x_i) + epsilon
            degree = (np.dot(np.transpose(np.subtract(x_j, x_i)), np.subtract(x, x_i))) / denominator
            degree=(np.abs(degree))
            if 0. <= degree <= 1.:
                projection_point = np.add(x_i, (np.dot(((np.dot(np.transpose(np.subtract(x_j, x_i)), np.subtract(x, x_i))) / denominator), (np.subtract(x_j, x_i)))))
                dist0.append(distance.euclidean(x, projection_point))
            else:
                dist0_euclidean.append(min(distance.euclidean(x_j, x), distance.euclidean(x_i, x)))


        if dist0:
            min_dist0 = min(dist0)
            dist0_sorted = sorted(dist0)

            if min_dist0 <= theta0:
                    y_pred.append(0)
                    TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN=pred_assing(0,TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN, y_test[j])

            if min_dist0 > theta0 and min_dist0 < theta2:
                    y_pred.append('w')

            elif min_dist0 >= theta2:
                    y_pred.append(1)
                    TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN=pred_assing(1,TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN, y_test[j])



        elif not dist0:
            min_dist0_euclidean = min(dist0_euclidean)
            if min_dist0_euclidean <= theta0:
                y_pred.append(0)
                TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN = pred_assing(0, TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN,
                                                                     y_test[j])

            if min_dist0_euclidean > theta0 and min_dist0_euclidean < theta2:
                y_pred.append('w')

            elif min_dist0_euclidean >= theta2:
                y_pred.append(1)
                TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN = pred_assing(1, TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN,
                                                                     y_test[j])


        all_distance_positive_train[:] = []
        dist0[:] = []
        dist0_euclidean[:] = []
        g0_weight_sorted[:]=[]
        sub_g0[:]=[]
        #if j < 6:
            #plot_boundaries.mayavi3d_mst(new_vectorize,small_mst0,small_mst1, theta, theta1, path,acc, unacc)

    try:
            end_time=time.time()
            print("Training one-class finished ",end_time - start_time)
            print("")
    except Exception as e:
            print(e)
            print(TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN)
            exit()

    #unacc[:] = []
    #acc[:] = []
    #acc_label[:] = []
    #unacc_label[:] = []
    #X_test[:] = []
    #y_test[:] = []
    # save_file('/home/super/PycharmProjects/rlagrassa/results/' + x_dataset + "_mst_cd_SM-GP",results_list)

    return y_pred

