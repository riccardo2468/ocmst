# -*- coding: utf-8 -*-
from __future__ import division  # to force division to floating point
import csv
import sys
import time
import codecs
import itertools
import math
import random
from collections import Counter
import argparse
from numpy import mean
from scipy.spatial import distance
import networkx as nx
import numpy as np
import torch
from sklearn.metrics import roc_auc_score
#import plot_boundaries
from metric import eucl_std_score
epsilon=sys.float_info.epsilon

def pairwise_distances(x, y):
    mmp1 = torch.stack([x] * y.size(0))
    mmp2 = torch.stack([y] * x.size(0)).transpose(0, 1)
    dist = torch.sqrt(torch.sum((mmp1 - mmp2) ** 2, 2))
    return dist


def pred_assing(pred, TP, TN_1, FP, FP_1, FN, FN_1,TP_1, TN, y_true):
    if pred == 0:
        if y_true == 0:
            TP = TP + 1
            TN_1 = TN_1 + 1
        elif y_true == 1:
            FP = FP + 1
            FN_1 = FN_1 + 1
    else:
        if y_true == 0:
            FP_1 = FP_1 + 1
            FN = FN + 1
        elif y_true == 1:
            TP_1 = TP_1 + 1
            TN = TN + 1
    return TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN




def create_small_mst(g0_weight_sorted, g1_weight_sorted, gamma_index, acc, unacc):
    small_g0 = nx.Graph()
    small_g0.add_nodes_from([i[0] for i in g0_weight_sorted[:gamma_index]])
    edges_couples = itertools.product(nx.nodes(small_g0), nx.nodes(small_g0))
    for u, v in (edges_couples):
        small_g0.add_edge(u, v, weight=distance.euclidean(acc[u], acc[v]))

    small_mst0 = nx.minimum_spanning_tree(small_g0)

    small_g1 = nx.Graph()
    small_g1.add_nodes_from([i[0] for i in g1_weight_sorted[:gamma_index]])
    edges_couples = itertools.product(nx.nodes(small_g1), nx.nodes(small_g1))
    for u, v in (edges_couples):
        small_g1.add_edge(u, v, weight=distance.euclidean(unacc[u], unacc[v]))

    small_mst1 = nx.minimum_spanning_tree(small_g1)
    return small_mst0, small_mst1


def main_bin_mst(acc,unacc,X_test,y_test, path, y_pred):
    results_list = []
    mean_accuracy_tosamplesx=[]
    acc_list = []
    threshold_list = [0.8]
    neighbours = [10]
    gamma=[15]

    for exp, gamma_index in zip(range(0, len(gamma)), gamma):
            print("#####################")
            print("Gamma: ",gamma[0],"Thr: ", threshold_list[0])
            g0 = nx.Graph()
            g1 = nx.Graph()

            for i in range(0, len(acc)):
                g0.add_node(i)

            for i in range(0, len(unacc)):
                g1.add_node(i)

            print("Dimension graph 0: Size g0: ", len(g0))
            print("Dimension graph 1: Size g1: ", len(g1))



            for k1 in neighbours:
                for alpha0 in threshold_list:
                    # prediction test
                    TN = 0
                    FP = 0
                    TP = 0
                    FN = 0

                    TN_1 = 0
                    FP_1 = 0
                    TP_1 = 0
                    FN_1 = 0
                    start_time=time.time()
                    for j in (range(0, len(X_test))):
                        if y_pred[j] == 'w':
                            new_vectorize = X_test[j]
                            all_distance_positive_train = []
                            all_distance_negative_train = []
                            sub_g0=[]
                            sub_g1=[]

                            tr=time.time()
                            dist_acc = pairwise_distances(torch.FloatTensor(acc).cuda(), torch.FloatTensor(X_test[j]).view(1, len(X_test[j])).cuda())
                            dist_unacc = pairwise_distances(torch.FloatTensor(unacc).cuda(),torch.FloatTensor(X_test[j]).view(1, len(X_test[j])).cuda())
                            dist_acc = dist_acc.cpu().tolist()
                            dist_unacc = dist_unacc.cpu().tolist()

                            new_acc, new_unacc = [], []
                            for tens_list in dist_acc:
                                for node0, t1 in enumerate(tens_list):
                                    new_acc.append([node0, t1])

                            for tens_list in dist_unacc:
                                for node0, t1 in enumerate(tens_list):
                                    new_unacc.append([node0, t1])



                            all_distance_positive_train.append(new_acc)
                            all_distance_negative_train.append(new_unacc)

                            # for node0 in nx.nodes(g0):
                            #     all_distance_positive_train.append([node0, distance.euclidean(new_vectorize, acc[node0])])
                            #
                            # for node1 in nx.nodes(g1):
                            #     all_distance_negative_train.append([node1, distance.euclidean(new_vectorize, unacc[node1])])


                            g0_weight_sorted = sorted(all_distance_positive_train[0], key=lambda x: x[1])

                            g1_weight_sorted = sorted(all_distance_negative_train[0], key=lambda x: x[1])


                            '''
                            #I refuse outliers
                            n_all_distance_p_train=sorted(all_distance_positive_train, key=itemgetter(1))
                            for a0 in n_all_distance_p_train[:]:
                                aux = [z[0] for z in ord_nodes0[:int(len(acc) * 0.95)]]
                                aux1 = [z[1] for z in ord_nodes0[:int(len(acc) * 0.95)]]
                                if a0[0] in aux or a0[0] in aux1:
                                    g0_weight_sorted.append(a0)
    
                            #I refuse outliers
                            n_all_distance_n_train = sorted(all_distance_negative_train, key=itemgetter(1))
                            for a1 in n_all_distance_n_train[:]:
                                aux=[z[0] for z in ord_nodes1[:int(len(unacc) * 0.95)]]
                                aux1 = [z[1] for z in ord_nodes1[:int(len(unacc) * 0.95)]]
                                if a1[0] in aux or a1[0] in aux1:
                                    g1_weight_sorted.append(a1)
    
    
                            index_min_positive_train = g0_weight_sorted[0][0]
    
                            index_min_negative_train = g1_weight_sorted[0][0]
    
                            mst0,mst1=create_small_mst(g0_weight_sorted,g1_weight_sorted,gamma_index,g0,g1)
    
                            #Search thresholds
                            dat = sorted(mst0.edges(data=True), key=itemgetter(2))
                            threshold = int(math.floor((mst0.size()) * alpha0))
                            theta = dat[threshold][2]['weight']
    
                            dat1 = sorted(mst1.edges(data=True), key=itemgetter(2))
                            threshold1 = int(math.floor((mst1.size()) * alpha0))
                            theta1 = dat1[threshold1][2]['weight']
                            '''

                            small_mst0, small_mst1 = create_small_mst(g0_weight_sorted, g1_weight_sorted, gamma_index, acc, unacc)
                            # print(small_mst0.edges(data=True))
                            # print([acc[nodes0] for nodes0 in nx.nodes(small_mst0)])
                            # print([unacc[nodes0] for nodes0 in nx.nodes(small_mst1)])
                            # print(new_vectorize)

                            #Search all edges incoming/outgoing to node (node is the minimum with our test  object)
                            # for edge0 in g0.edges(data=True):
                            #     if index_min_positive_train == edge0[0] or index_min_positive_train == edge0[1]:
                            #             sub_g0.append([edge0[0], edge0[1], edge0[2]['weight']])
                            #
                            # for edge1 in g1.edges(data=True):
                            #     if index_min_negative_train == edge1[0] or index_min_negative_train == edge1[1]:
                            #             sub_g1.append([edge1[0], edge1[1], edge1[2]['weight']])



                            #n_ary0, n_ary1 = create_n_ary(sub_g0, sub_g1, gamma_index)

                            # Search thresholds into small mst0/1
                            dat = sorted(small_mst0.edges(data=True), key=lambda x: x[2]['weight'])
                            threshold = int(math.floor((small_mst0.size()) * alpha0))
                            theta = dat[threshold][2]['weight']

                            dat1 = sorted(small_mst1.edges(data=True), key=lambda x: x[2]['weight'])
                            threshold1 = int(math.floor((small_mst1.size()) * alpha0))
                            theta1 = dat1[threshold1][2]['weight']

                            # search projection
                            dist0 = []
                            dist1 = []
                            dist0_euclidean = []
                            dist1_euclidean = []
                            for c0 in small_mst0.edges(data=True):
                                x_j = acc[c0[1]]
                                x_i = acc[c0[0]]
                                x = new_vectorize
                                #if c0[2]['weight'] <= theta:
                                denominator = distance.sqeuclidean(x_j, x_i) + epsilon
                                degree = (np.dot(np.transpose(np.subtract(x_j, x_i)), np.subtract(x, x_i))) / denominator
                                degree=(np.abs(degree))
                                if 0. <= degree <= 1.:
                                    projection_point = np.add(x_i, (np.dot(((np.dot(np.transpose(np.subtract(x_j, x_i)), np.subtract(x, x_i))) / denominator), (np.subtract(x_j, x_i)))))
                                    dist0.append(distance.euclidean(x, projection_point))
                                else:
                                    dist0_euclidean.append(min(distance.euclidean(x_j, x), distance.euclidean(x_i, x)))
                                #else:
                                    #dist0_euclidean.append(min(distance.euclidean(x_j, x), distance.euclidean(x_i, x)))

                            for c1 in small_mst1.edges(data=True):
                                x_j = unacc[c1[1]]
                                x_i = unacc[c1[0]]
                                x = new_vectorize
                                #if c1[2]['weight'] < theta1:
                                denominator = distance.sqeuclidean(x_j, x_i) + epsilon
                                degree = (np.dot(np.transpose(np.subtract(x_j, x_i)), np.subtract(x, x_i))) / denominator
                                degree = (np.abs(degree))
                                if 0. <= degree <= 1.:
                                    projection_point = np.add(x_i, (np.dot(((np.dot(np.transpose(np.subtract(x_j, x_i)), np.subtract(x, x_i))) / denominator),(np.subtract(x_j, x_i)))))
                                    dist1.append(distance.euclidean(x, projection_point))
                                else:
                                    dist1_euclidean.append(min(distance.euclidean(x_j, x), distance.euclidean(x_i, x)))

                                #else:
                                    #dist1_euclidean.append(min(distance.euclidean(x_j, x), distance.euclidean(x_i, x)))

                            if dist0 and dist1:
                                min_dist0 = min(dist0)
                                min_dist1 = min(dist1)
                                dist0_sorted = sorted(dist0)
                                dist1_sorted = sorted(dist1)

                                if min_dist0 <= theta and min_dist1 <= theta1:
                                    predicted=eucl_std_score(acc,unacc,g0_weight_sorted,g1_weight_sorted, k1, min_dist0, min_dist1)
                                    y_pred[j]=predicted
                                    TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN = pred_assing(predicted, TP, TN_1, FP, FP_1, FN, FN_1,
                                                                                         TP_1, TN, y_test[j])

                                if min_dist0 <= theta and min_dist1 > theta1:
                                        y_pred[j]=0
                                        TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN=pred_assing(0,TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN, y_test[j])


                                elif min_dist0 > theta and min_dist1 <= theta1:
                                        y_pred[j]=1
                                        TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN=pred_assing(1,TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN, y_test[j])

                                elif min_dist0 > theta and min_dist1 > theta1:
                                    predicted = eucl_std_score(acc, unacc, g0_weight_sorted, g1_weight_sorted, k1,
                                                               min_dist0, min_dist1)
                                    y_pred[j]=predicted
                                    TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN=pred_assing(predicted, TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN, y_test[j])


                            elif not dist0 and not dist1:
                                min_dist0_euclidean = min(dist0_euclidean)
                                min_dist1_euclidean = min(dist1_euclidean)
                                if min_dist1_euclidean <= theta1 and min_dist0_euclidean > theta:
                                        y_pred[j]=1
                                        TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN=pred_assing(1, TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN, y_test[j])


                                elif min_dist0_euclidean <= theta and min_dist1_euclidean > theta1:
                                        y_pred[j]=0
                                        TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN=pred_assing(0, TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN, y_test[j])


                                elif min_dist0_euclidean > theta and min_dist1_euclidean > theta1:
                                    predicted = eucl_std_score(acc, unacc, g0_weight_sorted, g1_weight_sorted, k1,min_dist0_euclidean, min_dist1_euclidean)
                                    y_pred[j]=predicted
                                    TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN=pred_assing(predicted, TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN, y_test[j])

                                elif min_dist0_euclidean <= theta and min_dist1_euclidean <= theta1:
                                    predicted = eucl_std_score(acc, unacc, g0_weight_sorted, g1_weight_sorted, k1,
                                                               min_dist0_euclidean, min_dist1_euclidean)
                                    y_pred[j]=predicted
                                    TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN=pred_assing(predicted, TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN, y_test[j])



                            else:
                                try:
                                    min_dist0 = min(dist0)
                                    dist0_sorted = sorted(dist0)

                                except (ValueError, TypeError):
                                    min_dist0 = None

                                try:
                                    min_dist1 = min(dist1)
                                    dist1_sorted = sorted(dist1)
                                except (ValueError, TypeError):
                                    min_dist1 = None

                                try:
                                    min_dist0_euclidean = min(dist0_euclidean)

                                except (ValueError, TypeError):
                                    min_dist0_euclidean = None

                                try:
                                    min_dist1_euclidean = min(dist1_euclidean)

                                except (ValueError, TypeError):
                                    min_dist1_euclidean = None

                                if min_dist0 is not None and min_dist1_euclidean is not None:
                                    if min_dist0 <= theta and min_dist1_euclidean > theta1:
                                        y_pred[j] = 0
                                        TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN=pred_assing(0, TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN, y_test[j])




                                    elif min_dist0 > theta and min_dist1_euclidean <= theta1:
                                        y_pred[j] = 1
                                        TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN=pred_assing(1, TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN, y_test[j])



                                    elif min_dist0 > theta and min_dist1_euclidean > theta1:
                                        predicted = eucl_std_score(acc, unacc, g0_weight_sorted, g1_weight_sorted, k1,
                                                                   min_dist0, min_dist1_euclidean)
                                        y_pred[j] = predicted
                                        TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN=pred_assing(predicted, TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN, y_test[j])


                                    elif min_dist0 <= theta and min_dist1_euclidean <= theta1:
                                        predicted = eucl_std_score(acc, unacc, g0_weight_sorted, g1_weight_sorted, k1,
                                                                   min_dist0, min_dist1_euclidean)
                                        y_pred[j] = predicted
                                        TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN=pred_assing(predicted, TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN, y_test[j])


                                elif min_dist1 is not None and min_dist0_euclidean is not None:
                                    if min_dist1 <= theta1 and min_dist0_euclidean > theta:
                                        y_pred[j] = 1
                                        TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN=pred_assing(1, TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN, y_test[j])


                                    elif min_dist1 > theta1 and min_dist0_euclidean <= theta:
                                        y_pred[j] = 0
                                        TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN=pred_assing(0, TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN, y_test[j])


                                    elif min_dist0_euclidean > theta and min_dist1 > theta1:
                                        predicted = eucl_std_score(acc, unacc, g0_weight_sorted, g1_weight_sorted, k1,
                                                                   min_dist0_euclidean, min_dist1)
                                        y_pred[j] = predicted
                                        TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN=pred_assing(predicted, TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN, y_test[j])


                                    elif min_dist0_euclidean <= theta and min_dist1 <= theta1:
                                        predicted = eucl_std_score(acc, unacc, g0_weight_sorted, g1_weight_sorted, k1,
                                                                   min_dist0_euclidean, min_dist1)
                                        y_pred[j] = predicted
                                        TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN=pred_assing(predicted, TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN, y_test[j])

                            all_distance_positive_train[:] = []
                            all_distance_negative_train[:] = []
                            dist0[:] = []
                            dist1[:] = []
                            dist0_euclidean[:] = []
                            dist1_euclidean[:] = []
                            g0_weight_sorted[:]=[]
                            g1_weight_sorted[:]=[]
                            sub_g0[:]=[]
                            sub_g1[:]=[]
                            #if j < 6:
                                #plot_boundaries.mayavi3d_mst(new_vectorize,small_mst0,small_mst1, theta, theta1, path,acc, unacc)

                    try:
                            end_time=time.time()
                            acc0 = (TP + TN) / (TP + TN + FN + FP)
                            acc1 = (TP_1 + TN_1) / (TP_1 + TN_1 + FN_1 + FP_1)

                            Sensitivity0 = TP / (TP + FN)
                            Precision0 = TP / (TP + FP)

                            Sensitivity1 = TP_1 / (TP_1 + FN_1)
                            Precision1 = TP_1 / (TP_1 + FP_1)

                            f0_score = (2 * (1 / ((1 / Precision0) + (1 / Sensitivity0))))
                            f1_score = (2 * (1 / ((1 / Precision1) + (1 / Sensitivity1))))
                            # '''contingency_table=np.array([[FP + FN + FP_1 + FN_1, TP + TN + FP_1 + FN_1],
                            #                            [FP + FN + TP_1 + TN_1, TP + TN + TP_1 + TN_1]])'''
                            #
                            # '''contingency_table = np.array([[FN + FN_1, TP + FN_1],
                            #                               [FN + TP_1, TP + TP_1]])
                            #
                            # chi2, p = mcnemar(ary=contingency_table, corrected=True)
                            # print('chi-squared:', chi2)
                            # print('p-value:', p)
                            # alpha_mc = 0.05
                            # if p > alpha_mc:
                            #     print('Same proportions of errors (fail to reject H0)')'''


                            conf_matrix=np.array([[TP + TP_1, FN + FN_1],[FP + FP_1, TN + TN_1]])
                            print("Time: ",end_time-start_time)
                            print(Sensitivity0, Sensitivity1, Precision0, Precision1, f0_score, f1_score)
                            print("Confusion matrix:")
                            print(conf_matrix)

                            details = [alpha0, None, k1, gamma_index, Sensitivity0, Sensitivity1, Precision0,
                                       Precision1, f0_score,
                                       f1_score, acc0]

                            acc_list.append(acc0)
                            results_list.append(details)
                            print("")
                            auc = 100 * roc_auc_score(y_test, y_pred)
                    except Exception as e:
                            print(e)
                            print(TP, TN_1, FP, FP_1, FN, FN_1, TP_1, TN)
                            exit()


    mean_accuracy_tosamplesx.append(mean(acc_list))
    print("Auc score: ", auc)
    return auc
    #plot.plot_line_test(mean_accuracy_tosamplesx,tr_size_list,"Accuracy",datasets_name)

# Load dataset
def initialization(acc, train_labels, test_data, test_labels, deep_comparison_flag, path, y_pred):
    if not deep_comparison_flag:
        print("Loading deep features...")
        parser = argparse.ArgumentParser()
        parser.add_argument('--path', type=str)
        parser.add_argument('--dir', type=str)
        parser.add_argument('--gpu', type=str, default='0')
        args = parser.parse_args()
        try:
            train_data = np.load(args.path + 'features.npy')
            train_labels = np.load(args.path + 'labels.npy')

            test_data = np.load(args.path + 'all_test_features.npy')
            test_labels = np.load(args.path + 'all_test_labels.npy')

            unacc = []
            acc = []
            acc_label = []
            unacc_label = []
            print("Datasets: ", args.path, "Loaded")
        except Exception as e:
            print(e)
            print("Loading datasetes failed")
            exit()

    unacc = []
    unacc_label = []



    for object, label in zip(test_data,y_pred):
        if label == 1:
            unacc.append(object)
            unacc_label.append(1)

    print("Size train class: ", len(acc), "Size alien class: ", len(unacc),
          "Test size: ", y_pred.count('w'))
    print("#########################################################################################################")
    return main_bin_mst(acc, unacc, test_data, test_labels, path, y_pred)

