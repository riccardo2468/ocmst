import torch.nn as nn
import torch.nn.functional as func



class LeNet(nn.Module):
    def __init__(self, num_classes):
        super(LeNet, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 2)
        self.fc2 = nn.Linear(2,num_classes)

    def forward(self, x):
        x = self.pool(func.relu(self.conv1(x)))
        x = self.pool(func.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = func.relu(self.fc1(x))
        logits = func.relu(self.fc2(x))
        preds_cluster = self.fc3(logits)
        preds = self.fc4(preds_cluster)
        return logits, preds_cluster, preds


