import os.path
import random
from matplotlib import pyplot as plt
import os
import pickle
import numpy as np
import PIL.Image
import itertools
import torch
import numpy
import torchvision
from PIL import Image
#from scipy.ndimage import imread
from torch.utils.data import DataLoader, SequentialSampler
from torchvision.datasets.utils import download_url, check_integrity
from utils import zip_varlen



class Cifar10(object):
    def __init__(self, batch_size, use_gpu, percentage_used, path, pop):
        #path = '/home/super/datasets/ferramenta52-multimodal/train'
        #path = '/home/super/PycharmProjects/rlagrassa/center_loss_mst_based/geometric_shapes'
        trainloader=[]
        train_labels=[]
        testloader=[]
        cluster0_labels, cluster1_labels,cluster0_labels_test, cluster1_labels_test=[],[],[],[]
        class_string_test=[]
        test_labels=[]
        self.batch_size = batch_size
        self.percentage_used = percentage_used
        self.path=path
        self.pop=pop

        train_transforms = torchvision.transforms.Compose([
            #torchvision.transforms.Resize(size=(32, 32)),
            # torchvision.transforms.Resize(size=100),  # Let smaller edge match
            # torchvision.transforms.RandomRotation(10),
            # torchvision.transforms.RandomCrop(32),
            # torchvision.transforms.CenterCrop(32),
            # torchvision.transforms.RandomHorizontalFlip(),
            # torchvision.transforms.RandomVerticalFlip(),
            torchvision.transforms.ToTensor()])

        test_transforms = torchvision.transforms.Compose([
            # torchvision.transforms.Scale(224),
            # torchvision.transforms.CenterCrop(224),
            #torchvision.transforms.Resize(size=(256,256)),
            # torchvision.transforms.CenterCrop(size=100),
            torchvision.transforms.ToTensor()])

        folders=(i for i in os.listdir(path+'/train'))
        folders=sorted(folders)
        class_idx={key: value for (key, value) in enumerate(folders)}
        print("Class list: ",class_idx)
        #pop=random.randint(0,9)
        class_train=class_idx[pop]
        print("Chosen class for trainining: ", class_train, pop)

        for index,key in enumerate(range(len(class_idx))):

            for index_en, i in enumerate(os.listdir(path + '/train/' + class_idx[key])):
                if index_en == (len(os.listdir(path+'/train/'+class_idx[key]))*50)/100 and index==pop:
                    break
                elif index_en == (len(os.listdir(path+'/train/'+class_idx[key]))*1)/100 and index!=pop:
                    break
                if i.endswith(".png") or i.endswith(".JPEG") or i.endswith(".jpg"):
                    try:
                        image=PIL.Image.open(path + '/train/' + class_idx[key] + '/' + i)
                        image_tensor = train_transforms(image)
                        if image_tensor.shape[0] == 3: #channel
                            trainloader.append(image_tensor)
                            if index == pop:
                                train_labels.append(0)
                            else:
                                train_labels.append(1)
                    except Exception as e:
                        pass
                    image.close()


        print(class_idx)



        #TODO for train/test splitted
        for index,key in enumerate(range(len(class_idx))):
            for index_img, i in enumerate(os.listdir(path+'/test/'+class_idx[key])):
                if index_img == (len(os.listdir(path + '/test/' + class_idx[key])) * 100) / 100 and index == pop:
                    break
                elif index_img == (len(os.listdir(path + '/test/' + class_idx[key])) * 100) / 100 and index != pop:
                    break
                if i.endswith(".png") or i.endswith(".JPEG") or i.endswith(".jpg"):
                    try:
                        image=PIL.Image.open(path+'/test/' + class_idx[key] + '/' + i)
                        image_tensor = test_transforms(image)
                        if image_tensor.shape[0] == 3: #channels
                            testloader.append(image_tensor)
                            if index == pop:
                                test_labels.append(0)
                            else:
                                test_labels.append(1)
                    except Exception as e:
                        pass
                    image.close()




        c = list(zip(trainloader, train_labels))
        random.shuffle(c)
        train_data, train_labels = zip(*c)

        train_data=torch.stack(train_data)
        train_data,train_labels=torch.tensor(train_data), torch.tensor(train_labels)

        trainloader = torch.split(train_data, batch_size)
        train_labels = torch.split(train_labels, batch_size)



        c = list(zip(testloader, test_labels))
        random.shuffle(c)
        testloader, test_labels = zip(*c)

        testloader=torch.stack(testloader)
        test_data, test_labels = torch.tensor(testloader), torch.tensor(test_labels)
        testloader = torch.split(test_data, batch_size)
        test_labels = torch.split(test_labels, batch_size)





        self.trainloader = trainloader
        self.train_labels= train_labels
        self.testloader = testloader
        self.test_labels = test_labels
        self.num_classes = 2
        print("Train size: ", len(trainloader)*batch_size, " Dataset percentage used: ", percentage_used * 100, "% ","Classes: ", self.num_classes, "Test loader size: ", len(testloader)*batch_size)


class one_class_dataset(object):
    def __init__(self, batch_size, use_gpu, percentage_used, path, pop):
        #path = '/home/super/datasets/ferramenta52-multimodal/train'
        #path = '/home/super/PycharmProjects/rlagrassa/center_loss_mst_based/geometric_shapes'
        trainloader=[]
        train_labels=[]
        testloader=[]
        cluster0_labels, cluster1_labels,cluster0_labels_test, cluster1_labels_test=[],[],[],[]
        class_string_test=[]
        test_labels=[]
        self.batch_size = batch_size
        self.percentage_used = percentage_used
        self.path=path
        self.pop=pop

        train_transforms = torchvision.transforms.Compose([
            #torchvision.transforms.Resize(size=(32, 32)),
            torchvision.transforms.Resize(size=32),  # Let smaller edge match
            # torchvision.transforms.RandomRotation(10),
            # torchvision.transforms.RandomCrop(32),
            # torchvision.transforms.CenterCrop(32),
            # torchvision.transforms.RandomHorizontalFlip(),
            # torchvision.transforms.RandomVerticalFlip(),
            torchvision.transforms.ToTensor(),
            #torchvision.transforms.Normalize(mean=(0.4914, 0.4822, 0.4465), std=(0.2023, 0.1994, 0.2010))
            #torchvision.transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])

        test_transforms = torchvision.transforms.Compose([
            # torchvision.transforms.Scale(224),
            # torchvision.transforms.CenterCrop(224),
            torchvision.transforms.Resize(size=32),
            # torchvision.transforms.CenterCrop(size=100),
            torchvision.transforms.ToTensor(),
            #torchvision.transforms.Normalize(mean=(0.4914, 0.4822, 0.4465), std=(0.2023, 0.1994, 0.2010))
            #torchvision.transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        ])

        folders=(i for i in os.listdir(path+'/train'))
        folders=sorted(folders)
        class_idx={key: value for (key, value) in enumerate(folders)}
        print("Class list: ",class_idx)
        #pop=random.randint(0,9)
        class_train=class_idx[pop]
        print("Chosen class for trainining: ", class_train, pop)

        for index_en, i in enumerate(os.listdir(path + '/train/' + class_train)):
            if index_en == (len(os.listdir(path+'/train/'+ class_train))*100)/100:
                break
            if i.endswith(".png") or i.endswith(".JPEG") or i.endswith(".jpg"):
                try:
                    image=PIL.Image.open(path + '/train/' + class_train + '/' + i)
                    image_tensor = train_transforms(image)
                    if image_tensor.shape[0] == 3: #channel
                        trainloader.append(image_tensor)
                        train_labels.append(0)
                except Exception as e:
                    pass
                image.close()





        #TODO for train/test splitted
        for index,key in enumerate(range(len(class_idx))):
            for index_img, i in enumerate(os.listdir(path+'/test/'+class_idx[key])):
                if index_img == (len(os.listdir(path + '/test/' + class_idx[key])) * 100) / 100 and index == pop:
                    break
                elif index_img == (len(os.listdir(path + '/test/' + class_idx[key])) * 100) / 100 and index != pop:
                    break
                if i.endswith(".png") or i.endswith(".JPEG") or i.endswith(".jpg"):
                    try:
                        image=PIL.Image.open(path+'/test/' + class_idx[key] + '/' + i)
                        image_tensor = test_transforms(image)
                        if image_tensor.shape[0] == 3: #channels
                            testloader.append(image_tensor)
                            if index == pop:
                                test_labels.append(0)
                            else:
                                test_labels.append(1)
                    except Exception as e:
                        pass
                    image.close()



        c = list(zip(trainloader, train_labels))
        random.shuffle(c)
        train_data, train_labels = zip(*c)

        train_data=torch.stack(train_data)
        train_data,train_labels=torch.tensor(train_data), torch.tensor(train_labels)

        trainloader = torch.split(train_data, batch_size)
        train_labels = torch.split(train_labels, batch_size)



        c = list(zip(testloader, test_labels))
        random.shuffle(c)
        testloader, test_labels = zip(*c)

        testloader=torch.stack(testloader)
        test_data, test_labels = torch.tensor(testloader), torch.tensor(test_labels)
        testloader = torch.split(test_data, batch_size)
        test_labels = torch.split(test_labels, batch_size)





        self.trainloader = trainloader
        self.train_labels= train_labels
        self.testloader = testloader
        self.test_labels = test_labels
        self.num_classes = 2
        print("Train size: ", len(trainloader)*batch_size, " Dataset percentage used: ", percentage_used * 100, "% ","Classes: ", self.num_classes, "Test loader size: ", len(testloader)*batch_size)


class MyDataset(object):
    def __init__(self, batch_size, use_gpu, percentage_used, path):
        #path = '/home/super/datasets/ferramenta52-multimodal/train'
        #path = '/home/super/PycharmProjects/rlagrassa/center_loss_mst_based/geometric_shapes'
        trainloader=[]
        train_labels=[]
        testloader=[]
        cluster0_labels, cluster1_labels,cluster0_labels_test, cluster1_labels_test=[],[],[],[]
        class_string_test=[]
        test_labels=[]
        self.batch_size = batch_size
        self.percentage_used = percentage_used
        self.path=path

        train_transforms = torchvision.transforms.Compose([
            torchvision.transforms.Resize(size=(256, 256)),
            # torchvision.transforms.Resize(size=100),  # Let smaller edge match
            # torchvision.transforms.RandomRotation(10),
            # torchvision.transforms.RandomCrop(32),
            # torchvision.transforms.CenterCrop(32),
            # torchvision.transforms.RandomHorizontalFlip(),
            # torchvision.transforms.RandomVerticalFlip(),
            torchvision.transforms.ToTensor()])

        test_transforms = torchvision.transforms.Compose([
            # torchvision.transforms.Scale(224),
            # torchvision.transforms.CenterCrop(224),
            torchvision.transforms.Resize(size=(256,256)),
            # torchvision.transforms.CenterCrop(size=100),
            torchvision.transforms.ToTensor()])

        folders=(i for i in os.listdir(path))
        class_idx={key: value for (key, value) in enumerate(folders)}
        print("Class list: ",class_idx)

        #TODO temp run
        train_tmp='002.american-flag','016.boom-box','023.bulldozer','195.soda-can'
        class_idx={key: value for (key, value) in enumerate(train_tmp)}
        print("Tmp class list: ", class_idx)


        for index,key in enumerate(range(len(class_idx))):
            for index_en, i in enumerate(os.listdir(path + '/' + class_idx[key]+ '/')):
                if i.endswith(".png") or i.endswith(".JPEG") or i.endswith(".jpg"):
                    try:
                        image=PIL.Image.open(path + '/' + class_idx[key] + '/' + i)
                        image_tensor = train_transforms(image)
                        if image_tensor.shape[0] == 3: #channel
                            trainloader.append(image_tensor)
                            if index > 0:
                                train_labels.append(1)
                            else:
                                train_labels.append(index)
                    except Exception as e:
                        pass
                    image.close()

        split=(len(trainloader)*batch_size * 80)/100
        for index_en, i in enumerate(zip(trainloader,train_labels)):
            if index_en == split:
                break
            index=random.randint(0,len(trainloader)-1)
            testloader.append(trainloader[index])
            test_labels.append(train_labels[index])
            del trainloader[index]
            del train_labels[index]



        # #TODO for train/test splitted
        # for index,key in enumerate(range(len(class_idx))):
        #     for index_img, i in enumerate(os.listdir(path+'/test'+'/'+class_idx[key])):
        #         #if index_img == 50:
        #             #break
        #         if i.endswith(".png") or i.endswith(".JPEG") or i.endswith(".jpg"):
        #             try:
        #                 image=PIL.Image.open(path+'/test' + '/' + class_idx[key] + '/' + i)
        #                 image_tensor = test_transforms(image)
        #                 if image_tensor.shape[0] == 3: #channels
        #                     testloader.append(image_tensor)
        #                     test_labels.append(index)
        #             except Exception as e:
        #                 pass
        #             image.close()




        c = list(zip(trainloader, train_labels))
        random.shuffle(c)
        train_data, train_labels = zip(*c)

        train_data=torch.stack(train_data)
        train_data,train_labels=torch.tensor(train_data), torch.tensor(train_labels)

        trainloader = torch.split(train_data, batch_size)
        train_labels = torch.split(train_labels, batch_size)



        c = list(zip(testloader, test_labels))
        random.shuffle(c)
        testloader, test_labels = zip(*c)

        testloader=torch.stack(testloader)
        test_data, test_labels = torch.tensor(testloader), torch.tensor(test_labels)
        testloader = torch.split(test_data, batch_size)
        test_labels = torch.split(test_labels, batch_size)





        self.trainloader = trainloader
        self.train_labels= train_labels
        self.testloader = testloader
        self.test_labels = test_labels
        self.num_classes = 2

        print("Train size: ", len(trainloader)*batch_size, " Dataset percentage used: ", percentage_used * 100, "% ","Classes: ", self.num_classes, "Test loader size: ", len(testloader)*batch_size)


class only_test(object):
    def __init__(self, batch_size, use_gpu, path):
        testloader=[]
        cluster0_labels, cluster1_labels,cluster0_labels_test, cluster1_labels_test=[],[],[],[]
        class_string_test=[]
        test_labels=[]
        self.batch_size = batch_size
        self.path=path

        test_transforms = torchvision.transforms.Compose([
            # torchvision.transforms.Scale(224),
            # torchvision.transforms.CenterCrop(224),
            torchvision.transforms.Resize(size=(256,256)),
            # torchvision.transforms.CenterCrop(size=100),
            torchvision.transforms.ToTensor()])

        folders=(i for i in os.listdir(path+'/test'))
        class_idx={key: value for (key, value) in enumerate(folders)}
        print("Class list: ",class_idx)

        for index,key in enumerate(range(len(class_idx))):
            for index_img, i in enumerate(os.listdir(path+'/test'+'/'+class_idx[key])):
                #if index_img == 50:
                    #break
                if i.endswith(".png") or i.endswith(".JPEG") or i.endswith(".jpg"):
                    try:
                        image=PIL.Image.open(path+'/test' + '/' + class_idx[key] + '/' + i)
                        image_tensor = test_transforms(image)
                        if image_tensor.shape[0] == 3: #channels
                            testloader.append(image_tensor)
                            test_labels.append(index)
                            all_labels = i.split('_')
                            cluster0_labels_test.append(int(all_labels[0])) #TODO remember it!
                            cluster1_labels_test.append(int(all_labels[1]))
                    except Exception as e:
                        pass
                    image.close()


        c = list(zip(testloader, test_labels, cluster0_labels_test,cluster1_labels_test))
        random.shuffle(c)
        testloader, test_labels,cluster0_labels_test,cluster1_labels_test = zip(*c)

        testloader=torch.stack(testloader)
        test_data, test_labels,cluster0_labels_test,cluster1_labels_test = torch.tensor(testloader), torch.tensor(test_labels), torch.tensor(cluster0_labels_test), torch.tensor(cluster1_labels_test)
        testloader = torch.split(test_data, batch_size)
        test_labels = torch.split(test_labels, batch_size)
        cluster0_labels_test = torch.split(cluster0_labels_test, batch_size)
        cluster1_labels_test = torch.split(cluster1_labels_test, batch_size)

        self.testloader = testloader
        self.test_labels = test_labels
        self.num_classes = len(class_idx)
        self.cluster0_labels_test= cluster0_labels_test
        self.cluster1_labels_test= cluster1_labels_test

        print("Classes: ", self.num_classes, "Test loader set: ", len(testloader))

__factory = {
    'MyDataset': MyDataset,
    'only_test': only_test,
    'Cifar10': Cifar10,
    'one_class_dataset': one_class_dataset
}


def create(name, batch_size, use_gpu, percentage_used, path,pop):
    if name not in __factory.keys():
        raise KeyError("Unknown dataset: {}".format(name))
    return __factory[name](batch_size, use_gpu,percentage_used, path, pop)
