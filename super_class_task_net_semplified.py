import argparse
import datetime
import itertools
import os
import os.path as osp
import pickle
import random
import sys
import time
from collections import Counter
import matplotlib
import torchvision

import Binary_mst
import OcMst
from Resnet_original import resnet18
from lenet import LeNet
from utils import Logger, AverageMeter
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import numpy as np
import torch
import torch.backends.cudnn as cudnn
import torch.nn as nn
from torch.optim import lr_scheduler
import datasets_semplified
from center_loss_standard import CenterLoss

parser = argparse.ArgumentParser("Center Loss Example")
parser.add_argument('-d', '--dataset', type=str, default='mnist',
                    choices=['MyDataset','Cifar10','one_class_dataset'])
# optimization
parser.add_argument('--batch-size', type=int, default=128)
parser.add_argument('--lr', type=float, default=0.001, help="learning rate for model")
parser.add_argument('--lr_cent', type=float, default=0.001, help="learning rate for center loss")
parser.add_argument('--max-epoch', type=int, default=500)
parser.add_argument('--stepsize', type=int, default=20)
parser.add_argument('--gamma', type=float, default=0.5, help="learning rate decay")
parser.add_argument('--eval-freq', type=int, default=1)
parser.add_argument('--percentage_used', type=float, default=1.)
parser.add_argument('--print-freq', type=int, default=50)
parser.add_argument('--gpu', type=str, default='0')
parser.add_argument('--seed', type=int, default=1)
parser.add_argument('--use-cpu', action='store_true')
parser.add_argument('--save-dir', type=str, default='log')
parser.add_argument('--path', type=str, default='dataset')
parser.add_argument('--when_save_model', type=int, default='100', help="save model at epoch x")
parser.add_argument('--when_save_deep_feat', type=int, default='100', help="save deep feat at epoch x")
parser.add_argument('--plot', action='store_true', help="whether to plot features for every epoch")

# Parameters for mst-loss
parser.add_argument('--loss', type=str, default='Xent',help="loss function (CenterLoss, ContrastiveCenterLoss, ClassicContrLoss, Xent, ClassicCenterLoss")
parser.add_argument('--pretrained', type=str, default='False', help="load pretrained model")
parser.add_argument('--lambda0', type=float, default=1.)
parser.add_argument('--lambda1', type=float, default=1.)
parser.add_argument('--clustering', type=str, default='Birch')

args = parser.parse_args()

def pairwise_distances(x, y=None):
    '''
    Input: x is a Nxd matrix
           y is an optional Mxd matirx
    Output: dist is a NxM matrix where dist[i,j] is the square norm between x[i,:] and y[j,:]
            if y is not given then use 'y=x'.
    i.e. dist[i,j] = ||x[i,:]-y[j,:]||^2 is different to euclidean distance because we remove square root.. remember it!
    '''
    x_norm = (x ** 2).sum(1).view(-1, 1)
    y_t = torch.transpose(x, 0, 1)
    y_norm = x_norm.view(1, -1)
    dist = torch.sqrt(x_norm + y_norm - 2.0 * torch.mm(x, y_t))  # TODO sqrt removed
    return torch.clamp(dist, 0.0, np.inf)


def centeroidnp(arr, dim_feat):
    length = arr.shape[0]
    return np.array([(np.sum(arr[:, i]) / length) for i in range(dim_feat)])
    # sum_x = np.sum(arr[:, 0])
    # sum_y = np.sum(arr[:, 1])
    # return np.array([sum_x/length, sum_y/length])


def save_images(data, labels, index_centroids, class_string):
    index_centroids = np.concatenate(index_centroids)
    number_clusters = len(set(index_centroids))
    for i in range(number_clusters):
        if not osp.exists('Clusters_gpu' + args.gpu + '_/Cluster_' + str(i)):
            os.makedirs('Clusters_gpu' + args.gpu + '_/Cluster_' + str(i))
    for index, (img, label, idx_cent, cl_str) in enumerate(zip(data, labels, index_centroids, class_string)):
        plt.imsave('Clusters_gpu' + args.gpu + '_/Cluster_' + str(idx_cent) + '/img_' + str(cl_str) + "_" + str(
            index) + '_label_' + str(label), img.transpose(1, 2, 0),
                   format='png')  # astype(np.uint8) remember your data
        # torchvision.utils.save_image(torch.from_numpy(img), "Clusters/Cluster_"+str(idx_cent)+'/img_'+str(index)+'_label_'+str(label))


def save_list(list0, ):
    if not osp.exists(args.save_dir + '/accuracies_list'):
        os.makedirs(args.save_dir + '/accuracies_list')

    dirname = osp.join(args.save_dir, 'accuracies_list/')

    with open(dirname + args.loss + "_" + str(args.deep_level) + "_" + str(args.batch_size) + "_lr:" + str(
            args.lr) + "_lr_cent:" + str(args.lr_cent), 'wb') as fp:
        pickle.dump(list0, fp)


# def clustering_generalization(model, trainloader, num_classes, global_labels):
#     chunks = [[] for i in range(num_classes)]
#     class_tmp = [[] for i in range(num_classes)]
#     chunks_labels = [[] for i in range(num_classes)]
#     class_list = [[] for i in range(num_classes)]
#
#     for batch_idx, (data, labels, chunk_str) in enumerate(zip(trainloader, global_labels)):
#         data = torch.tensor(data).cuda()
#         features, _, _ = model(data)
#         #features,_ =model(data)
#
#         features = features.data.cpu().numpy()
#         # labels = labels.data.cpu().numpy()
#         data = data.data.cpu().numpy()
#
#         # start = time.time()
#         for instance, y, single_data, cl_str in zip(features, labels, data,
#                                                     chunk_str):  # list of 10 list containing splitted instances by labels
#             class_list[y].append(instance)
#             chunks[y].append(single_data)
#             chunks_labels[y].append(y)
#             class_tmp[y].append(cl_str)
#
#     centroid_full = []
#     index_centroids = []
#
#     for idx, group in enumerate(class_list):
#         cl_trained = clustering_algo.fit(group)
#
#         if args.clustering == 'Kmeans' or args.clustering == 'MiniBatchKMeans':
#             cent = cl_trained.cluster_centers_
#         elif args.clustering == 'Birch':
#             cent = cl_trained.subcluster_centers_
#             print(len(cent))
#             exit()
#
#         pred = cl_trained.predict(group)
#
#         index_centroids.append(pred + len(centroid_full))
#
#         for i in range(len(cent)):
#             centroid_full.append(cent[i])
#
#     global_chunk = (list(itertools.chain.from_iterable(chunks)))
#     class_string = (list(itertools.chain.from_iterable(class_tmp)))
#     global_labels = (list(itertools.chain.from_iterable(chunks_labels)))
#     index_centroids = (list(itertools.chain.from_iterable(index_centroids)))
#
#     c = list(zip(global_chunk, index_centroids, global_labels, class_string))
#     random.shuffle(c)
#     global_chunk, index_centroids, global_labels, class_string = zip(*c)
#     global_labels = torch.split(torch.tensor(global_labels), args.batch_size)
#     new_trainloader = torch.split(torch.tensor(global_chunk), args.batch_size)
#     index_centroids = torch.split(torch.tensor(index_centroids), args.batch_size)
#     class_string = torch.split(torch.tensor(class_string), args.batch_size)
#     return new_trainloader, index_centroids, centroid_full, global_labels, class_string


def counter_clusters(index_centroids, class_string):
    index_centroids = np.concatenate(index_centroids)
    print(10 * '*')
    D = {}
    for i in list(set(index_centroids)):
        D.setdefault(i, [])

    for i, j in zip(index_centroids, class_string):
        D[i].append(j)

    for i in D.items():
        tmp_d = Counter(i[1])
        tmp_len = len(i[1])
        print("Clusters ", i[0],
              ["Label " + str(i[0]) + " presence " + str((i[1] / tmp_len) * 100) + '%' for i in tmp_d.items()])
    print(10 * '*')


def relaxing_centers(epoch):
    if epoch < 20:
        return 1
    elif epoch < 30:
        return 2
    elif epoch < 40:
        return 3
    elif epoch < 80:
        return 10
    elif epoch >= 80:
        return 30



def multi_centers_computation_per_batches(model,data, num_classes,labels,dim_feat):
    chunks = [[] for i in range(num_classes)]
    new_chunks = []
    super_chunks_labels=[]
    centroid_full = []
    index_centroids = []
    chunks_labels = [[] for i in range(num_classes)]

    class_list = [[] for i in range(num_classes)]

    data = data.cuda()
    features, _ = model(data)
    features = features.data.cpu().numpy()
    data = data.data.cpu().numpy()

    for instance, y, single_data in zip(features, labels, data):
        class_list[y].append(instance)
        chunks[y].append(single_data)
        chunks_labels[y].append(y)



    # for index, (sub_classes0,sub_classes_labels) in enumerate(zip(class_list,class_tmp0)):
    #     for index_sub0, (sub_classes1,one_class_labels) in enumerate(zip(sub_classes0,sub_classes_labels)):
    #         for index_sub1, (sub_class1_feat,sub_class1_labels) in enumerate(zip(sub_classes1, one_class_labels)):
    #             if len(sub_class1_feat) != 0:
    #                 tmp_data = np.asarray(sub_class1_feat)
    #                 centroid_full.append(torch.tensor(centeroidnp(tmp_data, dim_feat)))  # this method is much fast than before
    #                 index_centroids.append(np.repeat(len(centroid_full)-1, len(sub_class1_feat)))
    #                 new_chunks.append(torch.tensor(chunks[index][index_sub0][index_sub1]))
    #                 super_chunks_labels.append(torch.tensor(chunks_labels[index][index_sub0][index_sub1]))
    #                 sub_chunks_labels0.append(torch.tensor(class_tmp0[index][index_sub0][index_sub1]))
    #                 sub_chunks_labels1.append(torch.tensor(class_tmp1[index][index_sub0][index_sub1]))

    for index, (class_feat, classes_labels) in enumerate(zip(class_list,chunks_labels)):
                if len(class_feat) != 0:
                    tmp_data = np.asarray(class_feat)
                    centroid_full.append(torch.tensor(centeroidnp(tmp_data, dim_feat)))
                    index_centroids.append(np.repeat(len(centroid_full)-1, len(class_feat)))
                    new_chunks.append(torch.tensor(chunks[index]))
                    super_chunks_labels.append(classes_labels)
                elif len(class_feat) == 1:
                    tmp_data = np.asarray(class_feat)
                    centroid_full.append(torch.tensor(tmp_data))
                    index_centroids.append(np.repeat(len(centroid_full) - 1, len(class_feat)))
                    new_chunks.append(torch.tensor(chunks[index]))

                # TODO Clustering approach for multi-centers
                # if len(sub_class1_feat) != 0:
                #     cl_trained = clustering.algo.fit(sub_class1_feat)
                #
                #     if args.clustering == 'Kmeans' or args.clustering == 'MiniBatchKMeans':
                #         cent = cl_trained.cluster_centers_
                #     elif args.clustering == 'Birch':
                #         cent = cl_trained.subcluster_centers_
                #
                #     pred = cl_trained.predict(sub_class1_feat)
                #
                #     index_centroids.append(pred + len(centroid_full))
                #
                #     for i in range(len(cent)):
                #         centroid_full.append(torch.tensor(cent[i]))
                #
                #     new_chunks.append(torch.tensor(chunks[index][index_sub][index_sub1]))
                #     super_chunks_labels.append(one_super_class_labels)
                #     sub_chunks_labels0.append(sub_class1_labels0)
                #     sub_chunks_labels1.append(sub_class1_labels1)
                #
                #
                # elif len(sub_class1_feat)==1:
                #     index_centroids.append(len(centroid_full))
                #     print(sub_class1_feat)
                #     print(sub_class1_feat[0])
                #     exit()
                #     centroid_full.append(torch.tensor(sub_class1_feat[0]))
                #     new_chunks.append(torch.tensor(chunks[index][index_sub][index_sub1]))
                #     super_chunks_labels.append(one_super_class_labels)
                #     sub_chunks_labels0.append(sub_class1_labels0)
                #     sub_chunks_labels1.append(sub_class1_labels1)



    global_chunk = (list(itertools.chain.from_iterable(new_chunks)))
    global_labels = (list(itertools.chain.from_iterable(super_chunks_labels)))
    index_centroids = (list(itertools.chain.from_iterable(index_centroids)))


    c = list(zip(global_chunk, index_centroids, global_labels))
    random.shuffle(c)
    global_chunk, index_centroids, global_labels = zip(*c)
    global_labels = torch.stack(global_labels)
    global_chunk = torch.stack(global_chunk)
    index_centroids = torch.tensor(index_centroids)
    return global_chunk, index_centroids, centroid_full, global_labels


def main():
    all_test_features, all_test_labels= [], []
    #ray.init()
    torch.manual_seed(args.seed)
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    use_gpu = torch.cuda.is_available()
    if args.use_cpu: use_gpu = False

    sys.stdout = Logger(osp.join(args.save_dir, 'log_' + args.dataset + ' gpu= ' + args.gpu + ' datasize: ' + str(
        args.percentage_used) + '.txt'))
    if not os.path.exists(args.save_dir+'/deep_features/'):
        os.mkdir(args.save_dir+'/deep_features/')


    if use_gpu:
        print("Currently using GPU: {}".format(args.gpu))
        print("Learning rate optimizer {:}  Batch-size {:} Clustering used {:}".format(args.lr,args.batch_size,args.clustering))
        print("Pretrained model: {:}".format(args.pretrained))
        # print("Parameters for mst-loss: Deep Level set {:} Multiplier {:} Percentage training used {:}".format(args.deep_level, args.multiplier, args.percentage_used))
        cudnn.benchmark = True
        torch.cuda.manual_seed_all(args.seed)
    else:
        print("Currently using CPU")
        exit()

    print("Creating dataset: {}".format(args.dataset))
    dataset = datasets_semplified.create(
        name=args.dataset, batch_size=args.batch_size, use_gpu=use_gpu,
        percentage_used=args.percentage_used, path=args.path, pop=0
    )

    #clustering = Cluster_pilot.create(args.clustering)

    trainloader, testloader, test_labels, global_labels = dataset.trainloader, dataset.testloader, dataset.test_labels, dataset.train_labels

    # print("Creating model: {}".format(args.model))

    if args.pretrained == 'True':
        print("Model loaded")
        model = torch.load('logXent' + '.pt')
    else:
        print("No one model loaded")
        # model = ResNet101_mod(num_classes=dataset.num_classes)
        # model_first_step= torch_resnet.resnet18(pretrained=True)
        #model = ResNet18_mod(num_classes=dataset.num_classes)
        model = resnet18(num_classes=dataset.num_classes,pretrained=False)
        #model = LeNet(num_classes=dataset.num_classes)
        dim_features=model.linear1.in_features

    if use_gpu:
        model = nn.DataParallel(model).cuda()
        # model_first_step= nn.DataParallel(model_first_step).cuda()

    criterion_xent = nn.CrossEntropyLoss().cuda()
    #criterion_xent_clusters0 = nn.CrossEntropyLoss().cuda()
    #criterion_xent_clusters1 = nn.CrossEntropyLoss().cuda()

    criterion_cent = None
    optimizer_centloss = None

    # if args.loss == 'ContrastiveCenterLoss':
    #     print("ContrastiveCenterLoss mst based set")
    #     print("Features dimension: ", dim_features)
    #     criterion_cent = ContrastiveCenterLoss(dim_hidden=dim_features, num_classes=dataset.num_classes,
    #                                            use_gpu=use_gpu)

    if args.loss == 'CenterLoss':
        print("CenterLoss set lambda= ",args.lambda0)
        print("Features dimension: ", dim_features)
        criterion_cent = CenterLoss(dim_hidden=dim_features, num_classes=dataset.num_classes, use_gpu=use_gpu,
                                    batch_size=args.batch_size)


    # if args.loss == 'ClassicCenterLoss':
    #     print("ClassicCenterLoss set")
    #     print("Features dimension: ", dim_features)
    #     criterion_cent = ClassicCenterLoss(dim_hidden=dim_features, num_classes=dataset.num_classes, use_gpu=use_gpu)

    # optimizer_model = torch.optim.SGD(model.parameters(), lr=args.lr, weight_decay=5e-4, momentum=0.9)
    # optimizer_model = torch.optim.Adam(model.parameters(), lr=args.lr)

    if criterion_cent is None:
        params = list(model.parameters())
    else:
        params = list(model.parameters()) + list(criterion_cent.parameters())

    optimizer = torch.optim.Adam(params, args.lr)


    if args.stepsize > 0:
        scheduler = lr_scheduler.StepLR(optimizer, step_size=args.stepsize, gamma=args.gamma)

    acc_list, acc_ocmst = [], []
    centroids = []
    start_time = time.time()
    for epoch in range(args.max_epoch):
        if epoch == args.when_save_model:  # Save trained model
            torch.save(model, args.save_dir + "/model_saved/" +args.loss+'_'+ str(args.max_epoch)+'_'+ args.gpu + '.pt')
            print("Model trained saved using: ", args.loss)
        if args.loss == 'CenterLoss':

            #trainloader, index_centroids, centroids, global_labels, class_string = multi_centers_computation(model,trainloader,dataset.num_classes,global_labels,class_string,num_sub_classes)

            all_features, all_labels=train(trainloader, global_labels, model, criterion_xent, criterion_cent,optimizer,
                  use_gpu, dataset.num_classes, epoch, centroids, dim_features)


        else:
            all_features, all_labels=train(trainloader, global_labels, model, criterion_xent, criterion_cent,
                  optimizer, use_gpu, dataset.num_classes, epoch, None, None, None, None, None, None)

        #adjust_learning_rate(optimizer, epoch)
        print("==> Epoch {}/{}".format(epoch + 1, args.max_epoch))

        if args.stepsize > 0: scheduler.step()

        if args.eval_freq > 0 and (epoch + 1) % args.eval_freq == 0 or (epoch + 1) == args.max_epoch:
            print("==> Test")
            #acc, err, acc1, err1,acc2, err2  = test(model, testloader, test_labels,cluster0_labels_test,cluster1_labels_test, use_gpu)
            acc, err, all_test_features, all_test_labels = test(model, testloader, test_labels, use_gpu,epoch)
            acc_list.append(acc)
            print("Accuracy (%): {}\t Error rate (%): {}".format(acc, err))
            acc_ocmst.append(Binary_mst.initialization(all_features, all_labels, all_test_features, all_test_labels, True, args.save_dir))
            if epoch > 20:
                plot_accuracy_epochs(acc_list, acc_ocmst, epoch)


    elapsed = round(time.time() - start_time)
    elapsed = str(datetime.timedelta(seconds=elapsed))
    print("Finished. Total elapsed time (h:m:s): {}".format(elapsed))
    #plot_accuracy_epochs(acc_list)
    #save_list(acc_list)


def adjust_learning_rate(optimizer, epoch):
    if epoch < 60:
        lr_model = args.lr

    elif epoch < 120:
        lr_model = args.lr * 0.1

    elif epoch < 180:
        lr_model = args.lr * 0.01

    else:
        lr_model = args.lr * 0.001

    for param_group in optimizer.param_groups:
        param_group['lr'] = lr_model


def train(trainloader, global_labels, model, criterion_xent, criterion_cent,
          optimizer, use_gpu, num_classes, epoch, centroids, dim_features):
    model.train()
    xent_losses = AverageMeter()
    cent_losses = AverageMeter()
    xent_clusters0 = AverageMeter()
    xent_clusters1 = AverageMeter()
    #losses = AverageMeter()
    index_centroids1 = []
    all_features, all_labels, all_data = [], [], []

    for batch_idx, (data, labels) in enumerate(zip(trainloader, global_labels)):

        data, index_centroids, centroids, labels = multi_centers_computation_per_batches(model,data, num_classes,labels,dim_features)

        batch_centers = []

        if args.loss == 'CenterLoss' or args.loss == 'ContrastiveCenterLoss':
            for i in index_centroids:  # old value Their_centers rembember!
                batch_centers.append(centroids[i.item()])


            batch_centers=torch.stack(batch_centers).float()

        if use_gpu:
            data, labels, index_centroids = data.cuda(), labels.cuda(), index_centroids.cuda()
            if args.loss == 'CenterLoss' or args.loss == 'ContrastiveCenterLoss':
                pass

        features, outputs = model(data)
        loss_xent = criterion_xent(outputs, labels)


        if args.loss == 'CenterLoss' or args.loss == 'ContrastiveCenterLoss':
            # CenterLoss.centers=nn.Parameter(torch.tensor(batch_centers).float().cuda())
            loss_cent = criterion_cent(features, labels, index_centroids, batch_centers)
            # if epoch > 4: #Activate our multi-centerLoss
            loss = loss_cent*args.lambda0 + loss_xent
            #loss = loss_cent * args.lambda0 + loss_xent

            # else:
            # loss = loss_xent #freeze our multi-centerloss
        if args.loss == 'ClassicCenterLoss':
            loss_cent = criterion_cent(features, labels)
            loss = loss_cent + loss_xent
            # optimizer_centloss.zero_grad()

        if args.loss == 'Xent':
            loss = loss_xent

        optimizer.zero_grad()
        loss.backward()

        # optimizer_model.step()

        # for param in criterion_cent.parameters():
        #    param.grad.data *= (args.lr_cent / (args.lr))

        if args.loss == 'CenterLoss' or args.loss == 'ContrastiveCenterLoss' or args.loss == 'ClassicCenterLoss' or args.loss == 'Xent':
            optimizer.step()

        #losses.update(loss.item(), labels.size(0))
        xent_losses.update(loss_xent.item(), labels.size(0))


        if args.loss == 'CenterLoss' or args.loss == 'ContrastiveCenterLoss' or args.loss == 'ClassicCenterLoss':
            cent_losses.update(loss_cent.item(), labels.size(0))

        if use_gpu:
            all_features.append(features.data.cpu().numpy())
            all_labels.append(labels.data.cpu().numpy())
            all_data.append(data.data.cpu().numpy())

            if args.loss == 'CenterLoss':
                index_centroids1.append(index_centroids.data.cpu().numpy())
        else:
            all_features.append(features.data.numpy())
            all_labels.append(labels.data.numpy())

        if (batch_idx + 1) % 10 == 0:
            if args.loss == 'CenterLoss' or args.loss == 'ContrastiveCenterLoss' or args.loss == 'ClassicCenterLoss':
                print(
                    "Batch {}\t CenterLoss {:.6f} ({:.6f}) CrossEntropy {:.6f} ({:.6f})".format(
                        batch_idx + 1,
                        cent_losses.val,
                        cent_losses.avg,
                        xent_losses.val,
                        xent_losses.avg
                    ))
            else:
                print("Batch {}\t CrossEntropy {:.6f} ({:.6f})".format(batch_idx + 1, xent_losses.val, xent_losses.avg))

    all_features = np.concatenate(all_features, 0)
    all_labels = np.concatenate(all_labels, 0)
    all_data = np.concatenate(all_data, 0)
    #centroids = np.asarray(centroids)

    if args.plot:
        if args.loss == 'CenterLoss' or args.loss == 'ContrastiveCenterLoss':
            plot_features(all_features, all_labels,all_cl_str0,all_cl_str1, num_classes, epoch, centroids, None, index_centroids1,num_sub_classes0, num_sub_classes1,prefix='train_' + args.gpu)
        elif args.loss == 'ClassicCenterLoss' or args.loss == 'Xent':
            pass
            #plot_features(all_features, all_labels, num_classes, epoch, None, None, all_labels,prefix='train_' + args.gpu)

    if epoch == args.when_save_deep_feat and args.loss == 'CenterLoss':
        np.save(args.save_dir+'/deep_features/features.npy', all_features)
        np.save(args.save_dir+'/deep_features/labels.npy', all_labels)
        print("Train Features extracted")
        #save_images(all_data, all_labels, index_centroids1, all_cl_str)

    return all_features, all_labels


def test(model, testloader, test_labels, use_gpu, epoch):
    all_test_features, all_test_labels=[],[]
    model.eval()
    correct, total, correct1, total1, correct2, total2 = 0, 0, 0, 0, 0, 0
    with torch.no_grad():
        for data, labels in zip(testloader, test_labels):
            if use_gpu:
                data, labels = data.cuda(), labels.cuda()
            features, predicted = model(data)
            _, predicted = torch.max(predicted.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
            #if epoch == args.when_save_deep_feat:
            if epoch >=0:
                all_test_features.append(features.data.cpu().numpy())
                all_test_labels.append(labels.data.cpu().numpy())

            ##correct1 += (predicted_clusters == torch.tensor([i + j*6 for i,j in zip(sub_labels,labels)]).cuda()).sum().item()

    #if epoch == args.when_save_deep_feat:
    if epoch >=0:
        all_test_features = np.concatenate(all_test_features, 0)
        all_test_labels = np.concatenate(all_test_labels, 0)
        np.save(args.save_dir + '/deep_features/all_test_features.npy', all_test_features)
        np.save(args.save_dir + '/deep_features/all_test_labels.npy', all_test_labels)
        print("Test Features extracted")

    acc = correct * 100. / total
    err = 100. - acc
    return acc, err, all_test_features, all_test_labels


def plot_accuracy_epochs(acc_list, acc_ocmst, epochs):
    if not osp.exists(args.save_dir+'/comparison_plot_'+args.gpu):
        os.mkdir(args.save_dir+'/comparison_plot_'+args.gpu)
    fig, ax = plt.subplots()
    ax.plot([int(i) for i in range(0, epochs+1)], acc_list, label='NN')
    ax.plot([int(i) for i in range(0, epochs+1)], acc_ocmst, label='OCmst')

    ax.set(xlabel='Epochs', ylabel='Accuracy',
           title='Accuracy over epochs')
    ax.grid()
    dirname = osp.join(args.save_dir)
    ax.legend(shadow=True, fancybox=True, borderpad=1, loc='upper left')
    fig.savefig(dirname + '/comparison_plot_'+args.gpu+'/' + str(epochs) + "_" + str(
        args.batch_size) + " lr:" + str(args.lr) + ".png")


def plot_features(features, labels,all_cl_str0, all_cl_str1, num_classes, epoch, centroids, sub_class_labels, index_centroids, num_sub_classes0,num_sub_classes1, prefix):
    if args.loss != 'Xent' and args.loss != 'ClassicCenterLoss' and args.loss != 'Contrastive_centerLoss':
        index_centroids = np.concatenate(index_centroids)
        index_centroids_set = list(set(index_centroids))
        labels_set = list(set(labels))
        sub_labels0_set = list(set(all_cl_str0))
        all_colors = len(set(index_centroids))
    else:
        all_colors = num_classes

    #colors = .6 * np.random.rand(all_colors, 3) + 0.4
    colors = ['blue', 'green', 'red', 'gray', 'cyan', 'orange']
    all_markers=['^','s']
    if args.loss == 'Xent':
        # colors = ['C0', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9']

        for label_idx in range(num_classes):
            plt.scatter(
                features[labels == label_idx, 0],
                features[labels == label_idx, 1],
                facecolor=colors[label_idx],
                marker=all_markers[label_idx],
                s=2,
            )


    if args.loss != 'Xent' and args.loss != 'ClassicCenterLoss' and args.loss != 'Contrastive_centerLoss':
        for label_idx in range(0,num_classes):
            for l1 in range(0,num_sub_classes0):
                for l2 in range(0,num_sub_classes1):
                    plt.scatter(
                        features[((labels == label_idx) & (l1 == all_cl_str0) & (l2 == all_cl_str1)), 0],
                        features[((labels == label_idx) & (l1 == all_cl_str0) & (l2 == all_cl_str1)), 1],
                        facecolor=colors[l1],
                        marker=all_markers[label_idx],
                        edgecolors=colors[l2],
                        s=3,
                    )


    # if centroids is not None:
    #     plt.scatter(
    #         centroids[:, 0],
    #         centroids[:, 1],
    #         c='black',
    #         s=2,
    #         marker='x'
    #     )

    # for index,i in enumerate(classical_centroids):
    #     plt.scatter(
    #         i[0],
    #         i[1],
    #         s=40,
    #         marker='o',
    #         c='black',
    #     )

    clusters = ["C"+str(i) for i in range(num_sub_classes0*num_sub_classes1*num_classes)]
    #plt.legend(clusters, loc='upper right')
    dirname = osp.join(args.save_dir, prefix)
    if not osp.exists(dirname):
        os.mkdir(dirname)
    save_name = osp.join(dirname, 'epoch_' + str(epoch + 1) + '.png')
    plt.savefig(save_name, bbox_inches='tight', dpi=200)
    plt.close()


def main_pretrained():
    torch.manual_seed(args.seed)
    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
    use_gpu = torch.cuda.is_available()
    if args.use_cpu: use_gpu = False

    sys.stdout = Logger(osp.join(args.save_dir, 'log_' + args.dataset + ' gpu= ' + args.gpu + ' datasize: ' + str(
        args.percentage_used) + '.txt'))

    if use_gpu:
        print("Currently using GPU: {}".format(args.gpu))
        cudnn.benchmark = True
        torch.cuda.manual_seed_all(args.seed)
    else:
        print("Currently using CPU")
        exit()

    if not os.path.exists(args.save_dir+'/deep_features/'):
        os.mkdir(args.save_dir+'/deep_features/')



    model=torchvision.models.googlenet(pretrained=True).cuda()
    modules = list(model.children())[:-1]
    new_classifier = nn.Sequential(*modules)
    #new_classifier = nn.Sequential(*list(model.classifier.children())[:-1])
    #model.classifier = new_classifier

    for i in range(0,10):
        acc_ocmst, acc_list, all_features, all_labels, all_test_features, all_test_labels = [], [], [], [], [], []
        print("Creating dataset: {}".format(args.dataset))
        dataset = datasets_semplified.create(
            name=args.dataset, batch_size=args.batch_size, use_gpu=use_gpu,
            percentage_used=args.percentage_used, path=args.path, pop=i,
        )
        trainloader, testloader, test_labels, global_labels = dataset.trainloader, dataset.testloader, dataset.test_labels, dataset.train_labels
        model.eval()

        for data in trainloader:
            features = model(data.cuda())

            all_features.append(features.data.cpu().numpy())
        all_features = np.concatenate(all_features, 0).tolist()
        all_labels = np.concatenate(global_labels, 0).tolist()

        for data in testloader:
            features = model(data.cuda())

            all_test_features.append(features.data.cpu().numpy())
        all_test_features = np.concatenate(all_test_features, 0).tolist()
        all_test_labels = np.concatenate(test_labels, 0).tolist()

        #acc, err, all_test_features, all_test_labels = test(model, testloader, test_labels, use_gpu, 0)
        #acc_list.append(acc)
        #print("Accuracy (%): {}\t Error rate (%): {}".format(acc, err))
        y_pred=OcMst.main_OcMst(all_features,all_test_features,all_test_labels, args.path)
        acc_ocmst.append(Binary_mst.initialization(all_features, all_labels, all_test_features, all_test_labels, True, args.path,y_pred))
        print("*"*20)




if __name__ == '__main__':
    #main()
    main_pretrained()