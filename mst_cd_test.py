# -*- coding: utf-8 -*-
from __future__ import division  # to force division to floating point
from os.path import join
from matplotlib import pyplot as plt
import numpy as np
import Binary_mst
import os
import torch
import torch.backends.cudnn as cudnn
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('--path', type=str)
parser.add_argument('--dir', type=str)
parser.add_argument('--gpu', type=str, default='0')
args = parser.parse_args()


def plot_features(features, labels, test, test_labels, num_classes):

    #colors = .6 * np.random.rand(all_colors, 3) + 0.4
    train_colors = ['blue', 'green']
    all_markers=['^','s']
    for label_idx in range(num_classes):
        plt.scatter(
            features[labels == label_idx, 0],
            features[labels == label_idx, 1],
            facecolor=train_colors[label_idx],
            marker=all_markers[label_idx],
            s=3,
        )


    for label_idx in range(0,num_classes):
                plt.scatter(
                    test[(test_labels == label_idx), 0],
                    test[(test_labels == label_idx), 1],
                    facecolor='black',
                    marker=all_markers[label_idx],
                    #edgecolors=colors[l2],
                    s=2,
                )


    #plt.legend(clusters, loc='upper right')
    dirname = join(args.dir)
    if not os.path.exists(dirname):
        os.mkdir(dirname)
    save_name = join(dirname, 'fig_' + '.png')
    plt.savefig(save_name, bbox_inches='tight', dpi=300)
    plt.close()


def save_file(path, results):
    try:
        outF = open(path, "a+")
        for i in results:
            outF.writelines(str(i))
            outF.write('\n')
        outF.close()
        print("File saved.")
    except Exception as e:
        print("Error save results function", e)

results_list = []
mean_accuracy_tosamplesx=[]
os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
use_gpu = torch.cuda.is_available()

if use_gpu:
    print("Currently using GPU: {}".format(args.gpu))
    cudnn.benchmark = True
    torch.cuda.manual_seed_all(1)
else:
    print("Currently using CPU")
    exit()

# Load dataset
try:
    train_data = np.load(args.path+'features.npy')
    train_labels = np.load(args.path + 'labels.npy')

    test_data = np.load(args.path + 'all_test_features.npy')
    test_labels = np.load(args.path + 'all_test_labels.npy')

    unacc = []
    acc = []
    acc_label = []
    unacc_label = []
    print("Datasets: ", args.path, "Loaded")
except Exception as e:
    print(e)
    print("Loading datasetes failed")
    exit()


unacc = []
acc = []
acc_label = []
unacc_label = []
#X_train, X_test = train_data[train_index], train_data[test_index]
#y_train, y_test = train_labels[train_index], train_labels[test_index]


for object, label in zip(train_data, train_labels):
    if label == 1:
        unacc.append(object)
        unacc_label.append(label)
    elif label == 0:
        acc.append(object)
        acc_label.append(label)


print("Size train class: ", len(acc), "Size alien class: ", len(unacc), "Label train: ", len(acc_label),
      "Label alien: ", len(unacc_label), "Test size: ", len(test_data), "Label size: ", len(test_labels))
print("#########################################################################################################")
Binary_mst.main_mst_cd_gp(acc, unacc, test_data, test_labels, args.path)
#plot_features(train_data, train_labels, test_data, test_labels, len(set(test_labels)))
